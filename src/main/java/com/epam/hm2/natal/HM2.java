package com.epam.hm2.natal;

import com.epam.hm2.natal.consumers.IPinyouCounter;
import com.epam.hm2.natal.extractors.IPinyouExtractor;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;

@Slf4j
public class HM2 {

    public static void main(String[] args) {
//        args = new String[]{".",
//                            "C:\\Users\\Natal_Kaplia\\Documents\\HM2\\bid.20130606.txt.bz2;C:\\Users\\Natal_Kaplia\\Documents\\HM2\\bid.20130607.txt.bz2"};
        if (args.length < 2) {
            log.error("2 arguments minumum expected: output inputfile1 [inputfile2]");
            System.exit(-1);
        }
        File output = new File(args[0]);
        if (output.isFile()) {
            log.error("output is a file, should be a directory");
            System.exit(-1);
        }
        if (!(output.exists() || output.mkdirs())) {
            log.error("output directory could not be created");
            System.exit(-1);
        }

        IPinyouCounter ctr = new IPinyouCounter();

        IPinyouExtractor extractor = null;
        try {
            extractor = new IPinyouExtractor(Arrays.copyOfRange(args, 1, args.length), ctr);
        } catch (FileNotFoundException e) {
            log.error("Input file not found", e);
            System.exit(-1);
        }
        long start = System.currentTimeMillis();
        extractor.extractParallel();
        try {
            try (FileWriter fw = new FileWriter(Paths.get(output.getAbsolutePath(), "bid_result.txt").toFile())) {
                fw.write(ctr.getTop100());
            }
        } catch (IOException e) {
            log.error("Writing output failed", e);
        }
        log.info(String.format("Time consumed for processing: %d", System.currentTimeMillis() - start));
        log.info(String.format("Top100 output written to file:%s", output.getAbsolutePath()));
    }


}
