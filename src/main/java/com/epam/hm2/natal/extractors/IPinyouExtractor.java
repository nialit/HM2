package com.epam.hm2.natal.extractors;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;

@Slf4j
public class IPinyouExtractor {

    private final AtomicLong total = new AtomicLong(0);
    private final int threads;
    private final Consumer<String> lineConsumer;
    private final String[] inputFiles;
    private boolean launched = false;

    private FileSystem getHdfs(String path) throws URISyntaxException, FileNotFoundException {
        Configuration conf = new Configuration();
        conf.set("fs.hdfs.impl", org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
        try {
            return FileSystem.newInstance(new URI(path), conf);
        } catch (IOException e) {
            log.error(String.format("FileSystem could not be initialized on file: %s", path), e);
            throw (FileNotFoundException) new FileNotFoundException().initCause(e);
        }
    }

    private String[] pathToFiles(String path) throws URISyntaxException, FileNotFoundException {
        Collection<String> files = new ArrayList<>();
        if (path.startsWith("hdfs")) {
            try {
                Path p = new Path(path);
                FileSystem hdfs = getHdfs(path);
                FileStatus[] fss = hdfs.globStatus(p);
                for (FileStatus fs : fss) {
                    if (fs.isFile()) {
                        files.add(fs.getPath().toUri().toString());
                    }
                }
            } catch (IOException e) {
                String err = String.format("Input file does not exist:%s", path);
                log.error(err);
                throw (FileNotFoundException) new FileNotFoundException().initCause(e);
            }
        } else {

            String globRegex = "[!*?]";
            if (path.split(globRegex).length > 0) {
                String baseDir = path.split(globRegex)[0];
                final PathMatcher
                    matcher =
                    FileSystems.getDefault().getPathMatcher("glob:" + path);
                try {
                    Files.walkFileTree(Paths.get(baseDir), new SimpleFileVisitor<java.nio.file.Path>() {
                        @Override
                        public FileVisitResult visitFile(java.nio.file.Path file, BasicFileAttributes attrs)
                            throws IOException {
                            if (matcher.matches(file)) {
                                File f = file.toFile();
                                if (f.isFile()) {
                                    files.add(file.toFile().getAbsolutePath());
                                }
                            }
                            return FileVisitResult.CONTINUE;
                        }

                        @Override
                        public FileVisitResult visitFileFailed(java.nio.file.Path file, IOException exc)
                            throws IOException {
                            return FileVisitResult.CONTINUE;
                        }
                    });
                } catch (IOException e) {
                    throw (FileNotFoundException) new FileNotFoundException().initCause(e);
                }
            }
            File file = new File(path);
            if (file.isDirectory()) {
                for (File file1 : file.listFiles()) {
                    files.add(file1.getAbsolutePath());
                }
            }
            if (!file.exists()) {
                String err = String.format("Input file:%s does not exist", path);
                log.error(err);
                throw new FileNotFoundException(err);
            } else {
                return new String[]{path};
            }
        }
        return files.toArray(new String[0]);
    }


    private InputStream getInputFromFile(String path) throws FileNotFoundException, URISyntaxException {
        if (path.startsWith("hdfs")) {
            try {
                FSDataInputStream in = getHdfs(path).open(new Path(path));
                return in;
            } catch (IOException e) {
                String err = String.format("Input file does not exist:%s", path);
                log.error(err);
                throw (FileNotFoundException) new FileNotFoundException().initCause(e);
            }
        } else {
            File file = new File(path);
            if (!file.exists() || !file.isFile()) {
                String err = String.format("Input file:%s does not exist", path);
                log.error(err);
                throw new FileNotFoundException(err);
            } else {
                return new FileInputStream(file);
            }

        }
    }

    public IPinyouExtractor(String[] input, Consumer<String> consumer) throws FileNotFoundException {
        Collection<String> inputFiles = new ArrayList<>();
        for (int i = 0; i < input.length; i++) {
            try {
                for (String path : pathToFiles(input[i])) {
                    getInputFromFile(path);
                    inputFiles.add(path);
                }
            } catch (URISyntaxException e) {
                //wrap URISyntaxException with FileNotFoundException for more concise API
                throw new FileNotFoundException("Wrong URI: " + input[i]);
            }
        }
        this.threads = Runtime.getRuntime().availableProcessors();
        this.lineConsumer = consumer;
        this.inputFiles = inputFiles.toArray(new String[0]);
    }

    /**
     * @return Amount of currently processed lines
     */
    public long getTotalLinesProcessed() {
        return total.get();
    }

    /**
     * Should be called once to begin processing.
     */
    synchronized public void extractParallel() {
        if (!launched) {
            launched = true;
        } else {
            return;
        }
        ExecutorService pool = Executors.newFixedThreadPool(threads);
        try {
            ExecutorCompletionService<String>
                ecs =
                new ExecutorCompletionService<>(pool);
            int launched = 0;
            for (String f : inputFiles) {
                launched++;
                ecs.submit(() -> {
                    total.addAndGet(extract(f));
                    return f;
                });
            }
            for (int i = 0; i < launched; i++) {
                try {
                    String s = ecs.take().get();
                    log.info(String.format("Task %s completed", s));
                } catch (InterruptedException e) {
                    log.error("Task interrupted");
                } catch (ExecutionException e) {
                    log.error("Task threw exception", e);
                }
            }
            log.info("All tasks finished");
            log.info("Total lines processed:" + total);
        } finally {
            pool.shutdown();
        }
    }

    /**
     * Processes input file, iterates it's lines, feeds them to consumer, increments counters
     *
     * @param f file path to process
     * @return amount of processed lines per file
     * @throws ArchiveException
     * @throws IOException
     * @throws CompressorException
     */
    private final String inputLineDelimiter = "\\r?\\n";
    private final int logPerLinesProcessed = 100000;

    private long extract(String f)
        throws ArchiveException, IOException, CompressorException {
        try (
            InputStream in = getInputStreamFromPath(f)) {
            Scanner s = new Scanner(in).useDelimiter(inputLineDelimiter);
            long fileLineCounter = 0;
            while (s.hasNext()) {
                lineConsumer.accept(s.next());
                if (++fileLineCounter % logPerLinesProcessed == 0) {
                    log.debug(String.format("File %s lines read: %d", f, fileLineCounter));
                }
            }
            return fileLineCounter;
        }
    }

    /**
     * Wraps input path with appropriate {@link InputStream}
     *
     * @param path path to the file
     * @return {@link InputStream} for the path
     * @throws FileNotFoundException if file not found
     * @throws CompressorException   file can't be decompressed
     */
    private InputStream getInputStreamFromPath(String path) throws FileNotFoundException, CompressorException {
        try {
            if (path.endsWith("bz2")) {
                return new CompressorStreamFactory().createCompressorInputStream("bzip2", getInputFromFile(path));
            } else {
                return getInputFromFile(path);
            }
        } catch (URISyntaxException e) {
            throw new FileNotFoundException("Wrong URI: " + path);
        }
    }
}
