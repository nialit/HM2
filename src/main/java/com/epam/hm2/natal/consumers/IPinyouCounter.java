package com.epam.hm2.natal.consumers;

import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

@Slf4j
public class IPinyouCounter implements Consumer<String> {

    private final Map<String, AtomicInteger> iPinyouIdMap = new ConcurrentHashMap<>();
    private final String top100StringLineFormat = "%s : %d\r\n";

    public String getTop100() {
        StringBuilder sb = new StringBuilder();
        //sort Map by value descending, limit 100 and print orderly
        iPinyouIdMap.entrySet().stream().sorted(
            (e1, e2) ->
                e1.getValue().get() > e2.getValue().get() ? -1 : e1.getValue().get() < e2.getValue().get() ? 1 : 0)
            .limit(100)
            .forEachOrdered(e -> sb.append(String.format(top100StringLineFormat, e.getKey(), e.getValue().get())));
        return sb.toString();
    }

    private final String inputStringColumnSplitter = "\\t";

    private String getIPinyouId(String str) {
        String[] item = str.split(inputStringColumnSplitter);
        //check string for validity
        return item.length > 2 ? item[2] : null;
    }

    @Override
    public void accept(String item) {
        if (item == null) {
            return;
        } else {
            item = getIPinyouId(item);
            if (item == null || "null".equals(item)) {
                return;
            }
        }
        //here we ensure that only one AtomicInteger can be put in map concurrently
        AtomicInteger ctr = iPinyouIdMap.get(item);
        if (ctr == null) {
            ctr = new AtomicInteger(0);
            AtomicInteger temp = iPinyouIdMap.putIfAbsent(item, ctr);
            if (temp != null) {
                ctr = temp;
            }
        }
        ctr.incrementAndGet();
    }
}
