import com.epam.hm2.natal.extractors.IPinyouExtractor;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collections;

public class ExtractorTest {

    static File tempReadFile;
    static final String inputTestString = "testString testString";
    static final int repeatTestStringTimes = 10;

    @BeforeClass
    public static void init() throws IOException {
        File temp = Files.createTempFile("tempReadFile", "txt").toFile();
        tempReadFile = temp;
        try (FileWriter fw = new FileWriter(tempReadFile)) {
            for (int i = 0; i < repeatTestStringTimes; i++) {
                fw.write(inputTestString + "\r\n");
            }
        }
    }

    @AfterClass
    public static void cleanup() {
        tempReadFile.delete();
    }

    @Test(expected = FileNotFoundException.class)
    public void InputFileNotFoundExceptionTest() throws FileNotFoundException {
        new IPinyouExtractor(new String[]{"."}, null);
    }

    @Test
    public void LineReaderTest() throws Exception {
        StringBuilder sb = new StringBuilder();
        IPinyouExtractor ext = new IPinyouExtractor(new String[]{tempReadFile.getAbsolutePath()}, sb::append);
        ext.extractParallel();
        Assert.assertEquals("Wrong amount of lines counted", repeatTestStringTimes, ext.getTotalLinesProcessed());
        Assert.assertEquals("Lines are read incorrectly",
                            String.join("", Collections.nCopies(repeatTestStringTimes, inputTestString)),
                            sb.toString());
    }
}
