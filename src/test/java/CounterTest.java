import com.epam.hm2.natal.consumers.IPinyouCounter;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;

public class CounterTest {

    private Collection<String> lines;
    private final String
        testLine1 =
        "6a3e096782dc7bd19425357819cd2eee\t20130606000106028\tVhdL15jyOvT2Qbj\tmozilla/5.0 (windows nt 6.1) applewebkit/537.36 (khtml, like gecko) chrome/27.0.1453.94 safari/537.36\t118.171.162.*\t393\t393\t1\ttrqRTu1YP5scFsf\t88ebc674ee0105cab5dbac0f0133e0f6\t\tmm_10075660_3500949_11453278\t950\t90\t0\t1\t0\t23d6dade7ed21cea308205b37594003e\t227\t3427\tnull\n";
    private final String
        testLine2 =
        "2967805418343b18dbac9072c5ae48da\t20130606000106028\tVh1OOJqkONjfG-l\tmozilla/4.0 (compatible; msie 7.0; windows nt 5.1; trident/4.0; .net clr 2.0.50727; .net clr 3.0.4506.2152; .net clr 3.5.30729)\t1.60.70.*\t65\t74\t1\ttrqRTv1oGQuIXqK4wJB\teea8b595bf21a6d3922ceeab9f96692c\t\tmm_30232185_2681382_11197079\t950\t90\t0\t1\t0\tc938195f9e404b4f38c7e71bf50263e5\t238\t3476\tnull\n";
    private final String
        testLine3 =
        "ef5a1277a95453b5ac1446f7a4a2f4e2\t20130606000106029\tVhKDP593DU29FMj\tMozilla/5.0 (Windows NT 6.1) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/537.1,gzip(gfe),gzip(gfe)\t180.155.148.*\t79\t79\t2\teF1gtp57dNFrw9Kbu-\tc2db7c180922ed158a9ae0a67313d8b3\t\t2284287735\t300\t250\t0\t0\t8\td881a6c788e76c2c27ed1ef04f119544\t238\t3358\tnull\n";
    private long testLine1Count;
    private long testLine2Count;
    private long testLine3Count;
    private final String topResultSplit = " : ";

    @Before
    public void init() {
        lines = new ArrayList<>();

        for (int i = 0; i < 500; i++) {
            lines.add(testLine1);
            testLine1Count++;
            if (i % 2 == 0) {
                lines.add(testLine2);
                testLine2Count++;
            }
            if (i % 3 == 0) {
                lines.add(testLine3);
                testLine3Count++;
            }

        }
    }

    private final String lineColumnSplitter = "\\t";
    private final String lineRowSplitter = "\\r\\n";
    private final int targetElementColumnIndex = 2;

    private String getPinyouId(String item) {
        return item.split(lineColumnSplitter)[targetElementColumnIndex];
    }

    @Test
    public void TopCountingTest() {
        IPinyouCounter ctr = new IPinyouCounter();
        lines.forEach(ctr);
        String[] topLines = ctr.getTop100().split(lineRowSplitter);
        String errMsg = "Wrong top counting result";
        Assert.assertEquals(errMsg, getPinyouId(testLine1), topLines[0].split(topResultSplit)[0]);
        Assert.assertEquals(errMsg, testLine1Count, (long) Long.valueOf(topLines[0].split(topResultSplit)[1]));
        Assert.assertEquals(errMsg, getPinyouId(testLine2), topLines[1].split(topResultSplit)[0]);
        Assert.assertEquals(errMsg, testLine2Count, (long) Long.valueOf(topLines[1].split(topResultSplit)[1]));
        Assert.assertEquals(errMsg, getPinyouId(testLine3), topLines[2].split(topResultSplit)[0]);
        Assert.assertEquals(errMsg, testLine3Count, (long) Long.valueOf(topLines[2].split(topResultSplit)[1]));
    }
}
